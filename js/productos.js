"use strict";
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { ref as refStorage, getStorage, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCWo7jU4J-0e5oBuNPjxlhRUSvG5m8Nq5M",
    authDomain: "prowebfinal.firebaseapp.com",
    projectId: "prowebfinal",
    storageBucket: "prowebfinal.appspot.com",
    messagingSenderId: "786782290532",
    appId: "1:786782290532:web:539a1e127f18392c5dd571",
    measurementId: "G-LYV1VT648L"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const products = document.querySelector("#contenido");
const db = getDatabase();
const storage = getStorage();

async function showProducts() {
    try {
        event.preventDefault();

        products.innerHTML = "";
        const dbref = ref(db, "productos");

        await onValue(dbref, snapshot => {
            snapshot.forEach(childSnapshot => {
                const childData = childSnapshot.val();

                if (childData.status === "0") {

                    products.innerHTML += `
                    <div>
                        <img width="250" height="250" src="${childData.url}" alt="">${childData.nombre}
                        <p class="descripcion">${childData.descripcion}</p>
                        <p class="precio">$${childData.precio}</p>
                        <input class="boton" type="submit" value="Comprar">
                    </div>`;
                }
            });
        });

        products.classList.remove("d-none");
    } catch (error) {
        console.error(error);
    }
}

window.addEventListener("DOMContentLoaded", showProducts);