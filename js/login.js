// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCWo7jU4J-0e5oBuNPjxlhRUSvG5m8Nq5M",
    authDomain: "prowebfinal.firebaseapp.com",
    projectId: "prowebfinal",
    storageBucket: "prowebfinal.appspot.com",
    messagingSenderId: "786782290532",
    appId: "1:786782290532:web:539a1e127f18392c5dd571",
    measurementId: "G-LYV1VT648L"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const btnIngresar = document.querySelector("#btnIngresar");

// Autentificación
async function iniciarSesion() {
	event.preventDefault();
	let usuario = document.getElementById("usuario").value;
	let contraseña = document.getElementById("contraseña").value;

	if (usuario == "" || contraseña == "") {
		alert("Complete los campos");
		return;
	}

	try {
		await signInWithEmailAndPassword(auth, usuario, contraseña)
        // Signed in
		alert("Bienvenido " + usuario);
        window.location.href = "admin.html";
	} catch (error) {
		alert("Usuario y o contraseña incorrectos");
	}
}

onAuthStateChanged(auth, async user => {
	if (user) {
		if (window.location.pathname === "/html/login.html") {
			window.location.href = "/html/admin.html";
		}
	} else {
		if (window.location.pathname === "/html/admin.html") {
			window.location.href = "/html/login.html";
		}
	}
});

btnIngresar.addEventListener("click", iniciarSesion);